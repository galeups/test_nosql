'use strict';

const crypto = require('crypto');
const thinky = require('thinky')({
    host: "192.168.0.30"
});
var type = thinky.type;
var r = thinky.r;

var User = thinky.createModel("Users", {
    id:    type.string(),
    login: type.string(),
    email: type.string(),
    pass:  type.string(),
    salt:  type.string().default(function() {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    }),
    hash:  type.string().default(function() {
        return crypto.createHmac('sha1', this.salt).update(this.pass).digest('hex');
    }),
    confirmPass: type.boolean().default(true)

}, {
    pk: "login"
});

var user = new User({
    login: "User3",
    name:  "Denis",
    pass:  "qwerty"
});

user.save().then(function(result) {
    var res = result;
    console.log(result);
});
